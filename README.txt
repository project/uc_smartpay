
 Requrements:
=================================
 * php >= 5
 * SoapClient object support
(SOAP support is built into PHP5 natively, so we can forgot having to download
an additional package off PEAR but if in some reason you don't have it then
probably you need to compile it with this instructions
http://php.net/manual/en/soap.installation.php)

 Installation:
=================================
 1) Copy & unpack to Drupal modules directory

 2) Enable module

 3) Add 'process credit cards' and 'manual payments' permission to merchant role
    This needs to be enabled to make refund or process terminal payment
    <SITE-URL>/admin/user/permissions
    Administer › User management

 4) Copy SmartPay Notification.wsdl to module directory. Check link on
    <SITE-URL>/admin/store

 5) Enable SmartPay as Ubercart method
    <SITE-URL>/admin/store/settings/payment/edit/methods
    Administer › Store administration › Configuration › Payment settings

 6) Setup SmartPay
    <SITE-URL>/admin/store/settings/payment/edit/gateways
    Administer › Store administration › Configuration › Payment settings

 7) Check status page and make sure everything is OK
    <SITE-URL>/admin/reports/status
    Reports > Status reports

 8) Setup (more info on the page)
    <SITE-URL>/admin/store/settings/payment/edit/gateways OR
    Store administration > Configuration > Payment settings > Payment gateways

 9) Test it (in TEST mode) and use it (in LIVE mode). Change this on settings
    page <SITE-URL>/admin/store/settings/payment/edit/gateways
    Administer › Store administration › Configuration › Payment settings


 Going LIVE
=================================
 Everything is describe in SmartPay document [Procedure Guide] > [Section F].
 Download here http://www.barclaycard.com/smartpay/documentation/


 @TODO
=================================
 * Add multi sub accounts support (priority)
 * Add COMM (Commercial) / MOTO (Mail order/Telephone order) account support (
   more complex form)
 * Add Hosted Payment Pages payment support
 * Add HTTP POST method for Notification settings (now only SOAP is supported)