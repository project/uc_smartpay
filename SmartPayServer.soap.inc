<?php
/**
 * SmartPay server
 */
class SmartPayServer extends SoapServer {
  // Server URL's (notification)
  const TEST_NOTIFICATION_WSDL = 'https://ca-test.barclaycardsmartpay.com/ca/services/Notification?wsdl';
  const LIVE_NOTIFICATION_WSDL = 'https://ca-live.barclaycardsmartpay.com/ca/services/Notification?wsdl';
  const NOTIFICATION_FILE = "Notification.wsdl";

  private $debug;
  private static $services = NULL; // SOAP URL services

  /**
   * Payment SOAP Service WSDL –
   */
  function __construct($wsdlFile, $classArray) {
    // smartpay client initialize data
//    $this->host     = variable_get('uc_smartpay_status', 'test');
    $this->debug    = variable_get('uc_smartpay_debug', TRUE);
//    $this->services = self::getServices(); // load all soap urls

    if($this->debug) {
      ini_set('soap.wsdl_cache_enabled', '0'); // only for developing time
    }

    parent::__construct($wsdlFile, $classArray);
  }
}

// Smart pay soap objects
class NotificationRequest {
    public $notificationItems;
    public $live;
}

class NotificationRequestItem {
    public $amount;
    public $eventCode;
    public $eventDate;
    public $merchantAccountCode;
    public $merchantReference;
    public $originalReference;
    public $pspReference;
    public $reason;
    public $success;
    public $paymentMethod;
    public $operations;
    public $additionalData;
}

class Amount {
    public $currency;
    public $value;
}
