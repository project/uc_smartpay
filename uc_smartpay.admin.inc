<?php

function uc_smartpay_refund_page($order) {
  return drupal_get_form('uc_smartpay_refund_form', $order);
}

function uc_smartpay_refund_form(&$form_state, $order) {
  $form = array();
  $block_refund_field = FALSE;
  $max_refund = _uc_smartpay_get_refund($order);
  if($max_refund <= 0 && !$_SESSION['refund_just_done']) {
    drupal_set_message(t('Nothing to refund'), 'warning');
    $block_refund_field = TRUE;
  } else {
    unset($_SESSION['refund_just_done']);
  }

  $form['amount'] = array(
    '#value' => theme('uc_smartpay_amount', $max_refund, variable_get('uc_currency_sign', '$')),
  );
  $form['refund'] = uc_textfield(t('Refund value'), $max_refund, TRUE, t('Enter amount to refund. Default equal to balance.'));
  $form['refund']['#disabled'] = $block_refund_field;
  $currency_position = variable_get('uc_sign_after_amount', FALSE) ? '#field_suffix' : '#field_prefix';
  $form['refund'][$currency_position] = variable_get('uc_currency_sign', '$');

  $form['order_id']   = array('#type' => 'value',  '#value' => $order->order_id);
  $form['order_uid']  = array('#type' => 'value',  '#value' => $order->uid);
  $form['max_refund'] = array('#type' => 'value',  '#value' => $max_refund);
  $form['submit']     = array('#type' => 'submit', '#value' => t('Refund'));

  return $form;
}

function _uc_smartpay_get_refund(&$order) {
  $payments = uc_payment_load_payments($order->order_id);
  $amount = 0;

  if ($payments !== FALSE) {
    // @todo check if it smartpay payment
    foreach ($payments as $index => $payment) {
      if($payment->method == _payment_method_data('credit', 'name')) {
        $amount += $payment->amount;
      }
    }
  }

  return round($amount, 2);
}

/**
 * implementation of hook form validation
 */
function uc_smartpay_refund_form_validate($form, &$form_state) {
  $values = (object) $form_state['values'];

  // #1 refund value needs to be numeric value
  if(!is_numeric(str_replace(',', '.', $values->refund))) {
    $message = 'You must input numeric value.';

  // #2 refund can not be lower or equal zero
  } elseif($values->refund <= 0) {
    $message = 'Refund can not be negative or zero value';

  // #3 refund can not exceeding the original sale amount
  } elseif($values->refund > $values->max_refund) {
    $message = 'Refund can not exceed the original sale amount';
  }

  if($message) {
    form_set_error('refund', t($message));
  } else {
    $form_state['values']['refund'] = round($form_state['values']['refund'], 2);
  }
}

/**
 * implementation of hook form submit
 */
function uc_smartpay_refund_form_submit($form, &$form_state) {
  module_load_include('inc', 'uc_smartpay', 'SmartPayClient.soap');
  $values = (object) $form_state['values'];
  $order = uc_order_load($values->order_id);

  try {
    $spClient = new SmartPayClient('payment');
    $spClient->prepareRequest('modification', $order, NULL, $values->refund)->refund()->checkResponse($order, $values);
    $_SESSION['refund_just_done'] = TRUE;
  } catch(SoapFault $e) {
    uc_smartpay_debug($e);
    uc_smartpay_set_message('Refund falied', 'error');
  }
}


function uc_smartpay_set_message($message, $status = 'status') {
  $message = theme('uc_smartpay_comment', t($message));
  drupal_set_message($message, $status);
}


//////////////////////// CANCLE OR REFUND //////////////////////////////////////
//function uc_smartpay_cancel_or_refund_page($order) {
//  return drupal_get_form('uc_smartpay_cancel_or_refund_form', $order);
//}


///////////////////// NOTIFICATION ////////////////////////////
/**
 * Receive and parse request from SmartPay system. Confirmation/negation of
 * each smartpay function.
 *
 * Use a locally cached version of the Notification.wsdl (switch to live)
 * to guarantee service uptime. However, Notification.wsdl should be regularly
 * updated. Check 'API Integration Guide' manual to find actual URL:
 * http://www.barclaycard.com/smartpay/documentation/
 */
function uc_smartpay_notification() {
  // firt load server class
  module_load_include('inc', 'uc_smartpay', 'SmartPayServer.soap');

  // check if wsdl file exist
  $path = drupal_get_path('module', 'uc_smartpay'). '/'. SmartPayServer::NOTIFICATION_FILE;
  if(!file_exists($path)) {
    $options = array(
      '%file' => $path,
      '!date' => date('l-jS-F-Y H:i:s')
    );
    watchdog('uc_smartpay', "SmartPay try to communicate with 'us' at !date but we can't handle it request due to lack of %file file. Read README.txt.", $options, WATCHDOG_ERROR);
    exit();
  }

  // array of supported types
  $classmap = array(
    'NotificationRequest' => 'NotificationRequest',
    'NotificationRequestItem' => 'NotificationRequestItem',
    'Amount' => 'Amount'
  );

  // create server and handle request
  $server = new SmartPayServer($path, array('classmap' => $classmap));
  $server->addFunction("sendNotification");
  $server->handle();
}

/**
 * SmartPay server handle only this function
 *
 * @param $request
 *   object contain all information about some previous request. Often
 *   pspReference can be found there
 */
function sendNotification($request) {
  # For some reason NotificationRequestItem is an array for multiple notifications
  # and a scalar for a single notification (rather than an array with length = 1)
  if(is_array($request->notification->notificationItems->NotificationRequestItem)) {
    foreach($request->notification->notificationItems->NotificationRequestItem as $item) {
      notificationItem($item);
    }
  } else {
    $item = $request->notification->notificationItems->NotificationRequestItem;
    notificationItem($item);
  }

  return array("notificationResponse" => "[accepted]");
}

function notificationItem($item) {
  // if debug mode
  if(variable_get('uc_smartpay_debug', 1)) {
    dd($item);
  }

  // update order if success
  if($item->pspReference && $item->merchantReference) {
    $comment_text = '';
    $merchant_ref = explode('-', $item->merchantReference); // @todo it's mean nobody can't change way that ref is generated
    $order = uc_order_load($merchant_ref[0]);
    if($item->success == 1) {
      if($order->data['smartpay']['psp_reference']['authorise'] == $item->pspReference) {
        unset($item->operations);
        unset($item->additionalData);
        if(!isset($order->data['smartpay']['notification'])) {
          // map to array before save
          $item = (array) $item;
          $item['amount'] = (array) $item['amount'];
          $order->data['smartpay']['notification'] = $item;
        }
        uc_order_save($order);

        $comment_text = t('Notification received (OK)');
      } else {
        $comment_text = t('Notification received (FAILD): pspReference not found');
      }
    } else {
      $comment_text = t('Notification received (FAILD): reason: %reason', array('%reason' => $item->reason));
    }

    if($comment_text && $order->order_id) {
      // save to log
      uc_order_log_changes($order->order_id, array(theme('uc_smartpay_comment' , $comment_text)));
    }
  }
}
