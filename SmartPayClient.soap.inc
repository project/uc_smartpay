<?php

/**
 * SmartPay client
 */
class SmartPayClient extends SoapClient {
  // Clinet URL's (payment=modification/iDeal/recurring)
  const UC_SMARTPAY_TEST_PAYMENT_LOCATION = 'https://pal-test.barclaycardsmartpay.com/pal/servlet/soap/Payment';
  const UC_SMARTPAY_TEST_PAYMENT_WSDL     = 'https://pal-test.barclaycardsmartpay.com/pal/Payment.wsdl';
  const UC_SMARTPAY_LIVE_PAYMENT_LOCATION = 'https://pal-live.barclaycardsmartpay.com/pal/servlet/soap/Payment';
  const UC_SMARTPAY_LIVE_PAYMENT_WSDL     = 'https://pal-live.barclaycardsmartpay.com/pal/Payment.wsdl';

//  // @todo
//  // iDeal
//  const UC_SMARTPAY_TEST_IDEAL_LOCATION = 'https://pal-test.barclaycardsmartpay.com/pal/servlet/soap/Ideal';
//  const UC_SMARTPAY_TEST_IDEAL_WSDL     = 'https://pal-test.barclaycardsmartpay.com/pal/servlet/soap/Ideal?wsdl';
//  const UC_SMARTPAY_LIVE_IDEAL_LOCATION = 'https://pal-live.barclaycardsmartpay.com/pal/servlet/soap/Ideal';
//  const UC_SMARTPAY_LIVE_IDEAL_WSDL     = 'https://pal-live.barclaycardsmartpay.com/pal/servlet/soap/Ideal?wsdl';
//
//  // recurring
//  const UC_SMARTPAY_TEST_RECURRING_LOCATION = 'https://pal-test.barclaycardsmartpay.com/pal/servlet/soap/Recurring';
//  const UC_SMARTPAY_TEST_RECURRING_WSDL     = 'https://pal-test.barclaycardsmartpay.com/pal/Recurring.wsdl';
//  const UC_SMARTPAY_LIVE_RECURRING_LOCATION = 'https://pal-live.barclaycardsmartpay.com/pal/servlet/soap/Recurring';
//  const UC_SMARTPAY_LIVE_RECURRING_WSDL     = 'https://pal-live.barclaycardsmartpay.com/pal/Recurring.wsdl';


  private $debug;
  private static $services = NULL; // SOAP URL services

  /**
   * SmarPay client constructor
   * Support only payment service
   *
   * @todo add iDeal and recurring services
   *
   * @param $serviceName
   *   only payment support
   */
  function __construct($serviceName) {
    // smartpay client initialize data
    $host           = variable_get('uc_smartpay_status', 'test');
    $this->debug    = variable_get('uc_smartpay_debug', TRUE);
    $this->services = self::getServices(); // load all soap urls

    if($this->debug) {
      ini_set('soap.wsdl_cache_enabled', '0'); // only for developing time
    }

    if(in_array($host, array('test', 'live'))) {
      $serviceURL = (object) $this->services[$host][$serviceName];
      $options = array(
        'location' => $serviceURL->location,
        'login'    => variable_get('uc_smartpay_merchant_login', ''), // merchant account login
        'password' => variable_get('uc_smartpay_merchant_password', ''), // merchant account password
        'trace'    => $this->debug ? 1 : 0,
        'encoding' => SOAP_LITERAL
      );

      // create SOAP client
      try {
        parent::__construct($serviceURL->wsdl, $options);
      } catch(SoapFault $e) {
        _uc_smartpay_error($e);
      }
    } else {
      drupal_set_message(UC_SMARTPAY_ERROR_MESSAGE, 'error');
      $watchdog_message = 'SmartPay status needs to be set up on test or live. Check payment gateway !link';
      $watchdog_vars = array('!link' => l('settings', 'admin/store/settings/payment/edit/gateways'));
      $watchdog_link = 'admin/store/settings/payment/edit/gateways';

      // is critical because its seems something really wrong goes on gateway settings pages
      watchdog('uc_smartpay', $watchdog_message, $watchdog_vars, WATCHDOG_CRITICAL, l($watchdog_link, $watchdog_link));
    }
  }

  /**
   * support SmartPay paymentService methods (all trigger by static parent
   * __soapCall() method)
   *   authorise - normal authorization
   *   authorise3d - 3D domain authorisation
   *   authoriseReferral - currently not in use
   *   balanceCheck
   *   cancel
   *   cancelOrRefund
   *   capture
   *   checkFraud
   *   directdebit
   *   fundTransfer
   *   refund
   *   refundWithData
   *
   * fundTransfer/refundWithData – Are methods for gaming merchant to payout
   * winnings to cardholders.
   *
   * @param $function_name
   *   SmartPay method to fire
   * @param $args
   *   contain array with all options $args[0] == first option
   *
   * @todo extend for rest of services (iDeal, recurring)
   */
  function __call($function_name, $args) {
    try {
      if(empty($this->functionRequest) || gettype($this->functionRequest) != 'array') {
        throw new Exception("Argument functionRequest is empty or is not array. Did you use prepareRequest() method first?");
      }
    } catch(Exception $exception) {
      _uc_smartpay_error($exception, $this->order_id);
    }

    try {
      $this->lastFunction = $function_name;
      $this->lastResponse = parent::__soapCall($function_name, array($this->functionRequest));
      uc_smartpay_debug($this->lastResponse, "{$function_name}() run via __soapCall()");

      return $this;
    } catch(SoapFault $exception) {
      if(!_uc_smartpay_error($exception, $this->order_id)) {
        return $this;
      }
    }
  }

  private function getServices() {
    return array(
      'test' => array(
        'payment' => array(
          'wsdl'     => self::UC_SMARTPAY_TEST_PAYMENT_WSDL,
          'location' => self::UC_SMARTPAY_TEST_PAYMENT_LOCATION
        ),
      ),
      'live' => array(
        'payment' => array(
          'wsdl'     => self::UC_SMARTPAY_LIVE_PAYMENT_WSDL,
          'location' => self::UC_SMARTPAY_LIVE_PAYMENT_LOCATION
        ),
      )
    );
  }

  /**
   * Prepare SOAP <SmartPayType>Request object
   *
   * @todo add following requst types
   *  BalanceCheck Request
   *  DirectDebit Request
   *  FundTransfer Request
   *
   *  all docs can be find at
   *  http://www.barclaycard.com/smartpay/documentation/
   */
  function prepareRequest($type, &$order, $result = NULL, $value = NULL) {
    // remember order_id so we can use it later
    $this->order_id = $order->order_id;

    // need to recallculate to total amount of smallest value in currency e.g. 2.2$ = 220c (items)
    $value = $value ? $value*100 : $order->order_total*100;

    switch($type) {
      case 'payment':
        $this->functionRequest = array(
          "paymentRequest" => array(
            "amount" => array(
              "value"    => $value,
              "currency" => $order->currency
            ),
            "card" => array(
              "cvc"         => $order->payment_details['cc_cvv'],
              "expiryMonth" => $order->payment_details['cc_exp_month'],
              "expiryYear"  => $order->payment_details['cc_exp_year'],
              "holderName"  => $order->payment_details['cc_owner'],
              "number"      => $order->payment_details['cc_number'],
            ),

            "merchantAccount"  => variable_get('uc_smartpay_merchant_account', ''),
            "reference"        => $order->data['smartpay']['merchant_reference'], // TODO based on _atd_backoffice_get_trans_id()
            "shopperIP"        => $_SERVER['REMOTE_ADDR'],
            "shopperEmail"     => $order->primary_email,
            "shopperReference" => $order->uid
          )
        );
        break;

      case 'modification':
        $this->functionRequest = array(
          "modificationRequest" => array(
            "additionalData" => array(),
            "modificationAmount" => array(
              "value"    => $value,
              "currency" => $order->currency
            ),
            "merchantAccount"  => variable_get('uc_smartpay_merchant_account', ''),
            "authorisationCode" => is_null($result) ? $order->data['smartpay']['auth_code'] : $result->authCode, // if NULL try get it from order object
            "originalReference" => is_null($result) ? $order->data['smartpay']['psp_reference']['authorise'] : $result->pspReference // if NULL try get it from order object
          )
        );
        break;
    }

    // exposing alteration functionality for other modules
    drupal_alter('uc_smartpay_function_request', $this->functionRequest);

    // if debug mode is on, print request data
    uc_smartpay_debug($this->functionRequest, 'functionRequest');

    return $this; // chaning
  }

  /**
   * Map Drupal UC_CREDIT_AUTH_ONLY txn type
   */
  function ucCreditAuthOnly(&$order, &$amount) {
    $this->prepareRequest('payment', $order, NULL, $amount)->authorise()->checkResponse($order);

    return $this->lastResponse->success;
  }

  /**
   * Map Drupal UC_CREDIT_PRIOR_AUTH_CAPTURE txn type
   */
  function ucCreditPriorAuthCapture(&$order, &$amount) {
    $this->prepareRequest('modification', $order, NULL, $amount)->capture()->checkResponse($order);

    return $this->lastResponse->success;
  }

  /**
   * Map Drupal UC_CREDIT_AUTH_CAPTURE txn type
   */
  function ucCreditAuthCapture(&$order, &$amount) {
    $this->prepareRequest('payment', $order, NULL, $amount)->authorise()->checkResponse($order);
    if($this->lastResponse->success) {
      // prepare request data and fire SOAP [capture]
      $result = $this->lastResponse->paymentResult;
      $this->prepareRequest('modification', $order, $result, $amount)->capture()->checkResponse($order); // fire SmartPay
    } else {
      // trigger error
    }

    return $this->lastResponse->success;
  }

  /**
   * Check results after SmartPay request
   *
   * @return boolean True if request was sucessful, False otherwise
   */
  function checkResponse(&$order, $values = NULL) {
    // check response against the last SOAP function
    switch($this->lastFunction) {
      // AUTHORIZE
      case 'authorise':
        // if authorization goes fine then prepare capture data
        //resultCode - The result of the payment: “Authorised”, “Refused”, “Error".
        switch($this->lastResponse->paymentResult->resultCode) {
          case 'Authorised':
            $this->lastResponse->success = TRUE;
            // save info in order log
            $comment = theme('uc_smartpay_comment' , t('authorization receive by the system'));
            uc_order_log_changes($order->order_id, array($comment));

            // save auth_code and psp_reference for further usage
            // @todo wrap in _uc_smartpay_order_save($order, $result);
            // or in ->orderSave($order, $result);
            $order->data = uc_credit_log_authorization($order->order_id, $this->lastResponse->paymentResult->authCode, $amount);
            $order->data['smartpay']['psp_reference']['authorise'] = $this->lastResponse->paymentResult->pspReference;
            $order->data['smartpay']['auth_code'] = $result->paymentResult->authCode;
            uc_order_save($order);

            // if additional data is enabled save CV2AVS information if exist
            if($this->lastResponse->paymentResult->additionalData->entry) {
              $CV2AVS = array();
              foreach($this->lastResponse->paymentResult->additionalData->entry as $entry) {
                if(in_array($entry->key, array('avsResult', 'cvcResult', 'cvcResultRaw'))) {
                  $CV2AVS[] = "{$entry->key}: {$entry->value}";
                }
              }

              if($CV2AVS) {
                $comment = theme('uc_smartpay_comment', 'CV2AVS: '. implode(' / ', $CV2AVS));
                uc_order_comment_save($order->order_id, 0, $comment, 'admin');
              }
            }

            // save fraud result if exist
            if($this->lastResponse->paymentResult->fraudResult) {
              $comment = theme('uc_smartpay_comment', 'Fraud score: '. $this->lastResponse->paymentResult->fraudResult->accountScore);
              uc_order_comment_save($order->order_id, 0, $comment, 'admin');
            }
            break;

          case 'Refused':
            $this->lastResponse->success = FALSE;
            $message = self::getRefusedReason($this->lastResponse->paymentResult->refusalReason);
            uc_smartpay_debug(t($message));
            break;

          case 'Error':
            $this->lastResponse->success = FALSE;
            uc_smartpay_debug('response : Error');
            break;

          default:
            $this->lastResponse->success = FALSE;
            uc_smartpay_debug("response: Unknow");
            // unknow result general error
            // @todo handle smartpay soap fault (code error)
        }
        break;

      case 'authorise3d':
        // @todo
        break;

      // CAPTURE
      case 'capture':
        if($this->lastResponse->captureResult->response == '[capture-received]') {
          $this->lastResponse->success = TRUE;

          // prepare order log message..
          $change_reason = 'capture received by the system';

          // ..and save phpReference for further usage
          $order->data['smartpay']['psp_reference']['capture'] = $this->lastResponse->captureResult->pspReference;
          uc_order_save($order);
        } else {
          // prepare failure message log
          $change_reason = 'capture failed';
          $this->lastResponse->success = FALSE;
        }
        $changes = array(theme('uc_smartpay_comment', t($change_reason)));
        uc_order_log_changes($order->order_id, $changes);
        break;

      // REFUND
      case 'refund':
        if($this->lastResponse->refundResult->response == '[refund-received]') {
          // set message
          $options = array('!refund' => '('. uc_currency_format($values->refund) . ')');
          $message = t('Refund request received by service successfully. !refund', $options);
          uc_smartpay_set_message($message);

          // save refund psp
          $order->data['smartpay']['psp_reference']['refund'] = $this->lastResponse->refundResult->pspReference;
          uc_order_save($order);

          // update order payment
          // @todo when we receive notification from SmartPay that refund was complite
          $amount = -1*$values->refund;
          $uid = $order->order_uid;
          _uc_smartpay_payment_enter($order->order_id, $amount, '[refund-received]', $uid);

        } else {
          // @todo add watchdog
        }
        break;

      // CANCEL
      case 'cancel':
        // @todo
        break;

      // CANCEL OR REFUND
      case 'cancelOrRefund':
        // @todo
        break;
    }

  }

  function getRefusedReason($code) {
    switch($code) {
      case 'Refused'          : return t('Declined by issuer (eg insufficient funds or not suitable for this use)'); break;
      case 'Referral'         : return 'Declined by issuer (card holder needs to contact bank)'; break;
      case 'Acquirer Error'   : return 'Declined because of an error with the acquirer'; break;
      case 'Blocked Card'     : return 'Declined because the card is blocked (e.g. lost or stolen)'; break;
      case 'Expired Card'     : return 'Declined because card is no longer valid'; break;
      case 'Invalid Amount'   : return 'Declined because amount is incorrect'; break;
      case 'Invalid Card Number': return 'Declined because card number does not exist'; break;
      case 'Issuer Unavailable': return 'Declined because of a system error at the issuer side'; break;
      case 'Fraud'            : return 'Declined by Barclaycard SmartPay due to fraud suspicion'; break;
    }
  }
}
