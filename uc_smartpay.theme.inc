<?php
/**
 * @file
 * Theme functions for UC SmartPay.
 */

/**
 * Theme the list of cards SmartPay accepts.
 *
 * @ingroup themeable
 */
function theme_uc_smartpay_cards() {
  $path_to_all_images = drupal_get_path('module', 'uc_smartpay') . '/images/';

  $card_types = array(
    'visa' => 'Visa',
    'mc' => 'MasterCard',
    'maes' => 'Maestro',
    'paypal' => 'PayPal',
    'money' => 'Moneybookers',
    'giropay' => 'giropay',
    'elv' => 'ELV',
    'ideal' => 'iDeal',
    'direct' => 'direct',
    'sofort' => 'Sofort',
    'banktransfer' => 'bank transfer',
  );

  foreach ($card_types as $card_type => $card_title) {
    $image_path = $path_to_all_images . 'card_'.$card_type . '.png';
    $cards[] = theme('image', $image_path, $card_title, $card_title, array('style' => 'position:relative; top:5px; margin-right:4px;'));
  }

  // TODO use CSS and theme('item_list')
  return '<span>' . implode('', $cards) . '</span>';
}

/**
 * @ingroup themeable
 */
function theme_uc_smartpay_logo() {
  $image_path = drupal_get_path('module', 'uc_smartpay') . '/images/powered-by-smartpay.png';
  return theme('image', $image_path, 'SmartPay', 'SmartPay', array('style' => 'position:relative; top:10px;'));
}

function theme_uc_smartpay_comment($comment) {
  return UC_SMARTPAY. ": {$comment}";
}