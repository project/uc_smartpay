<?php
function get_smartpay_error_code($code) {
  $smartpay_error_code = array(
    // code => (0) message, (1) description
    '000' => array('Unknown', 'An unknown error occurred'),
    '010' => array('Not allowed', 'You are not allowed to perform this action'),
    '100' => array('No amount specified', 'There is no amount specified in the request'),
    '101' => array('Invalid card number', 'The specified card number is not valid'),
    '102' => array('Unable to determine variant', 'The system was not able to determine the variant of the card number'),
    '103' => array('CVC is not the right length', 'The length of the CVC code is not correct for the given card number'),
    '104' => array('Billing address problem', 'There was an error in the specified billing address fields'),
    '105' => array('Invalid PaRes from issuer', 'The submitted PaRes (Internet Authentication Response) from the issuer is not correct'),
    '107' => array('Recurring is not enabled', 'Recurring is not configured on the merchant account'),
    '108' => array('Invalid bankaccount number', 'The specified bankaccount number is not valid'),
    '109' => array('Invalid variant', 'The determined variant of the card is not valid'),
    '110' => array('BankDetails missing', 'No bank details specified'),
    '111' => array('Invalid BankCountryCode specified', 'The specified bankCountryCode is not valid (bank payment)'),
    '112' => array('This bank country is not  supported  The specified bankCountryCode is not  supported (bank payment)'),
    '117' => array('Invalid billing addresss', 'The specified billing address is not valid'),
    '125' => array('Invalid recurring contract specified', 'The specified recurring contract value is not valid'),
    '126' => array('Bank Account or Bank Location Id not valid or missing', 'The specified bank account or bank location Id is not valid or missing (elv payment)'),
    '127' => array('Account holder missing', 'No account holder specified'),
    '128' => array('Card Holder Missing', 'No card holder specified'),
    '129' => array('Expiry Date Invalid', 'The specified expiry data is not a valid date'),
    '134' => array('Billing address problem (Country) E.g country missing', 'Not all address information is being supplied. If you chose to supply address fields, you must supply all address fields.'),
    '137' => array('Invalid amount specified', 'The specified amount is invalid, or above the equivalent of 50,000.00 EUR.'),
    '138' => array('Unsupported currency specified', 'The specified currency is not supported'),
    '139' => array('Recurring requires shopperEmail and shopperReference', 'No shopperEmail or shopperReference specified'),
    '140' => array('Invalid expiryMonth[1..12] / expiryYear[>2000], or before now', 'The specified expiry month/year is not valid or in the past'),
    '141' => array('Invalid expiryMonth[1..12] / expiryYear[>2000]', 'The specified expiry month/year is not valid'),
    '142' => array('Bank Name or Bank Location not valid or missing', 'The specified bank name or bank location Id is not valid or missing (elv payment)'),
    '144' => array('Invalid startMonth[1..12] / startYear[>2000], or in the future', 'The specified start month/year is not valid or in the future'),
    '800' => array('Contract not found', 'The specified recurring contract could not be found'),
    '802' => array('Invalid contract', 'The specified recurring contract is not valid'),
    '803' => array('PaymentDetail not found', 'The specified paymentdetails could not be found'),
    '804' => array('Failed to disable', 'Unable to disable the specified recurring details'),
    '805' => array('RecurringDetailReference not available for provided recurringcontract', 'The specified recurringDetailReference is not available for the specified recurring contract'),
    '901' => array('Invalid Merchant Account', 'The specified merchant account is not valid'),
    '902' => array('Shouldn\'t have gotten here without a request!', 'No request specified, or invalid namespace'),
    '903' => array('Internal error', 'An internal error occurred while processing the request'),
    '904' => array('Unable To Process', 'The request could not be processed'),
    '905' => array('Payment details are not supported', 'The specified payment details are not available for the specified action')
  );

  return $smartpay_error_code[$code];
}